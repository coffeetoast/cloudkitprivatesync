//
//  AppDelegate.swift
//  SimpleCloudKitPrivate
//
//  Created by SungJae Lee on 2017. 3. 15..
//  Copyright © 2017년 SungJae Lee. All rights reserved.
//

import UIKit
import CloudKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var subscriptionIsLocallyCached: Bool {
        set {
            _ = UserDefaults.standard.set(newValue, forKey: "subscriptionIsLocallyCached")
        }
        get {
            let userDefaults = UserDefaults.standard.bool(forKey: "subscriptionIsLocallyCached")
            return userDefaults
        }
    }
    
    let privateDB = CKContainer.default().privateCloudDatabase
    var privateDBChangeToken: CKServerChangeToken?
    var privateDBFetchChangeToken: CKServerChangeToken?

    var changedZoneId = [CKRecordZoneID]()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        window?.rootViewController = ViewController()
        registerNotification(application)
        fetchChanges{
            self.subscribeToChange()
        }
        return true
    }
    
    fileprivate func subscribeToChange() {
        if (subscriptionIsLocallyCached) { return }
        
        let subscription = CKDatabaseSubscription(subscriptionID: "PrivateDB-changes")
        
        let notificationInfo = CKNotificationInfo()
        notificationInfo.shouldSendContentAvailable = true
        subscription.notificationInfo = notificationInfo
        
        // save cksubscription to server...
        let operation = CKModifySubscriptionsOperation(subscriptionsToSave: [subscription], subscriptionIDsToDelete: nil)
        operation.modifySubscriptionsCompletionBlock = {
            _,_,error in
            guard error == nil else { print(error!); return }
            
            self.subscriptionIsLocallyCached = true
            print("completed save subscription....")
        }
        
        operation.qualityOfService = .utility
        
        
        privateDB.add(operation)

    }
    
    fileprivate func registerNotification(_ application: UIApplication) {
        UIApplication.shared.registerForRemoteNotifications()
        UNUserNotificationCenter.current().delegate = self
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("------------------ didreceiveRemoteNoti")
        let dict = userInfo
        let notification = CKNotification(fromRemoteNotificationDictionary: dict)
        
        if(notification.subscriptionID == "PrivateDB-changes") {
            fetchChanges{
                completionHandler(.newData)
            }
        }
    }
    
    fileprivate func fetchChanges(_ callback: @escaping () -> Void){
        //print("ServerToken : \(privateDBChangeToken)")
        let changesOperation = CKFetchDatabaseChangesOperation(previousServerChangeToken: privateDBChangeToken)
        
        changesOperation.fetchAllChanges = true
        changesOperation.recordZoneWithIDChangedBlock = {
            changedZoneId in
            self.changedZoneId.append(changedZoneId)
        }
        changesOperation.recordZoneWithIDWasDeletedBlock = {
            deletedZoneId in
            if let index = self.changedZoneId.index(of: deletedZoneId) {
                self.changedZoneId.remove(at: index)
            }
        }
        
        changesOperation.changeTokenUpdatedBlock = { serverChangeToken in
            self.privateDBChangeToken = serverChangeToken
        }
        
        changesOperation.fetchDatabaseChangesCompletionBlock = {
            newToken, more, error in
            guard error == nil else { print(error!); return }
            
            self.privateDBChangeToken = newToken
            //print("ServerToken : \(self.privateDBChangeToken)")

            self.fetchZoneChanges(callback)
        }
        
        self.privateDB.add(changesOperation)
    }
    
    fileprivate func fetchZoneChanges(_ callback: @escaping () -> Void) {
        
        let changeRecordOp = CKFetchRecordZoneChangesOperation(recordZoneIDs: self.changedZoneId, optionsByRecordZoneID: self.optionDic())
        changeRecordOp.fetchAllChanges = true
        changeRecordOp.recordChangedBlock = {
            record in
            print("\(record) is changed.")
        }
        changeRecordOp.recordWithIDWasDeletedBlock = {
            recordId,_ in
            print("\(recordId) is deleted.")
        }
        changeRecordOp.recordZoneChangeTokensUpdatedBlock = {
            recordZoneId, changedToken, _ in
            self.privateDBFetchChangeToken = changedToken
        }
        changeRecordOp.recordZoneFetchCompletionBlock = {
            recordZoneId, changedToken, _,_, error in
            guard error == nil else {print("1 - \(error!)");return}
            
            self.privateDBFetchChangeToken = changedToken
           // print("ServerToken : \(self.privateDBFetchChangeToken)")
            print("successfully All fetched in \(recordZoneId).")
        }
        changeRecordOp.fetchRecordZoneChangesCompletionBlock = {
            error in
            guard error == nil else {
                print("2 - \(error!.localizedDescription)"); return
            }
            
            print("All fetchrecordZoneChanges Finished!")
            callback()
        }
        self.privateDB.add(changeRecordOp)
    }
    
    fileprivate func optionDic() -> [CKRecordZoneID:CKFetchRecordZoneChangesOptions] {
       let result = self.changedZoneId.map { element -> ((CKRecordZoneID), CKFetchRecordZoneChangesOptions) in
            let options = CKFetchRecordZoneChangesOptions()
            options.previousServerChangeToken = self.privateDBFetchChangeToken
            return (element, options)
        }
        var dic = [CKRecordZoneID: CKFetchRecordZoneChangesOptions]()
        for element in result {
            dic[element.0] = element.1
        }
        //print(dic)
        return dic
        
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate
{
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        print("------------------ didreceiveRemoteNoti in iOS 10")
//        let dict = response.notification.request.content.userInfo
//        let notification = CKNotification(fromRemoteNotificationDictionary: dict)
//        
//        if(notification.subscriptionID == "PrivateDB-changes") {
//            fetchChanges{
//                completionHandler()
//            }
//        }
//    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([])
    }
}

