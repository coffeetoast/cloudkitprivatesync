//
//  ViewController.swift
//  SimpleCloudKitPrivate
//
//  Created by SungJae Lee on 2017. 3. 15..
//  Copyright © 2017년 SungJae Lee. All rights reserved.
//

import UIKit
import CloudKit

class ViewController: UIViewController {
    let customZone = CKRecordZone(zoneName: "NoteZone")
    let privateDB = CKContainer.default().privateCloudDatabase

    var todeleteArray = [CKRecordID]()

    let button: UIButton = {
       let bt = UIButton(type: .system)
        bt.setTitle("Add Note", for: .normal)
        bt.addTarget(self, action: #selector(ViewController.handleButton), for: .touchUpInside)
        return bt
    }()
    
    let button2: UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("delete All", for: .normal)
        bt.addTarget(self, action: #selector(ViewController.handleButton2), for: .touchUpInside)
        return bt
    }()
    
    let button3: UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("query", for: .normal)
        bt.addTarget(self, action: #selector(ViewController.handleButton3), for: .touchUpInside)
        return bt
    }()
    
    let debugConsol: UITextView = {
        let dc = UITextView()
        dc.backgroundColor = .black
        dc.textColor = .white
        dc.isEditable = false
        dc.isSelectable = false
        dc.text = ""
        return dc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.backgroundColor = .white
        setupViews()
        //saveCustomZone()
        //queryStuff()
    }
    
    fileprivate func setupViews() {
        view.addSubview(button)
        view.addSubview(button2)
        view.addSubview(button3)
        view.addSubview(debugConsol)
        
        button.anchorCenterXToSuperview(constant: -80)
        button.anchorCenterYToSuperview()
        
        button2.anchorCenterXToSuperview(constant: 0)
        button2.anchorCenterYToSuperview()
        
        button3.anchorCenterXToSuperview(constant: 80)
        button3.anchorCenterYToSuperview()
        
        debugConsol.anchor(button2.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 10, rightConstant: 10, widthConstant: 0, heightConstant: 0)
    }
    
//    fileprivate func saveCustomZone() {
//        let newZoneOp = CKModifyRecordZonesOperation(recordZonesToSave: [self.customZone], recordZoneIDsToDelete: nil)
//        newZoneOp.modifyRecordZonesCompletionBlock = {
//            savedZoneArray, _, error in
//            guard error == nil else {print(error!); return }
//
//            print("Successfully Save Zone - \(savedZoneArray?.first?.zoneID)")
//        }
//        
//        privateDB.add(newZoneOp)
//    }


}

extension ViewController
{
    func handleButton() {
        let note = CKRecord(recordType: "Note", zoneID: customZone.zoneID)
        note.setValue("\(arc4random_uniform(10))", forKey: "text")
        
        let saveOperation = CKModifyRecordsOperation(recordsToSave: [note], recordIDsToDelete: nil)
        saveOperation.modifyRecordsCompletionBlock = {
            record, _ ,error in
            guard error == nil else {print(error!); return }
            print("\(record) is successflly saved........")

            DispatchQueue.main.async {
                let a = String(format: "%@ is successflly saved........\n", record!)
                self.debugConsol.text = self.debugConsol.text + a

            }
            //self.queryStuff()

        }
        
        privateDB.add(saveOperation)
        
// delete All Note..........
//        let predicate = NSPredicate(value: true)
//        let query = CKQuery(recordType: "Note", predicate: predicate)
//        // Create the initial query operation
//        
//        let queryOperation = CKQueryOperation(query: query)
//        queryOperation.recordFetchedBlock = {
//            record in
//            //print(record)
//            self.todeleteArray.append(record.recordID)
//        }
//        queryOperation.queryCompletionBlock = { _, error in
//            guard error == nil else {print(error!); return}
//         
//            print("query complete.")
//        }
//        
//        privateDB.add(queryOperation)
    }
    
    func handleButton2() {
        
        let saveOperation = CKModifyRecordsOperation(recordsToSave: nil, recordIDsToDelete:  self.todeleteArray)
        
        saveOperation.modifyRecordsCompletionBlock = {
            _, recordId ,error in
            guard error == nil else {print(error!); return }
            print("\(recordId) is successflly deleted........")

            DispatchQueue.main.async {

            let a = String(format: "%@ is successflly deleted........\n", recordId!)
            self.debugConsol.text = self.debugConsol.text + a
            }
            self.todeleteArray = [CKRecordID]()
        }
        
        privateDB.add(saveOperation)

    }
    
    func handleButton3() {
        self.todeleteArray = [CKRecordID]()

        let predicate = NSPredicate(value: true)
        let query = CKQuery(recordType: "Note", predicate: predicate)
        // Create the initial query operation
        
        let queryOperation = CKQueryOperation(query: query)
        queryOperation.recordFetchedBlock = {
            record in
            //print(record)
            self.todeleteArray.append(record.recordID)
        }
        queryOperation.queryCompletionBlock = { _, error in
            guard error == nil else {print(error!); return}
            print("================")
            print("total Data: \(self.todeleteArray.count)")
            print("================")
            
            print("query complete.")
            
            DispatchQueue.main.async {

                let a = "================\n"
                let b = String(format: "total Data: %d.\n", self.todeleteArray.count)
                let c = "================\n"
                let d = "query complete.\n"
                self.debugConsol.text = self.debugConsol.text + a + b + c + d
            }
        }
        
        privateDB.add(queryOperation)
    }

}


//fileprivate func queryStuff() {
//    self.todeleteArray = [CKRecordID]()
//    let predicate = NSPredicate(value: true)
//    let query = CKQuery(recordType: "Note", predicate: predicate)
//    // Create the initial query operation
//    
//    let queryOperation = CKQueryOperation(query: query)
//    queryOperation.recordFetchedBlock = {
//        record in
//        //print(record)
//        self.todeleteArray.append(record.recordID)
//    }
//    queryOperation.queryCompletionBlock = { _, error in
//        guard error == nil else {print(error!); return}
//        print("================")
//        print(self.todeleteArray )
//        print("================")
//        
//        print("query complete.")
//    }
//    
//    privateDB.add(queryOperation)
//}

